# README #

### LIBROS APP ###

* Aplicacion web desarrollada con Angular que muestra una tabla con los libros registrados en la base de datos y permite realizar el alta de nuevos libros.
* Version 1.1


## Instrucciones de desarrollo ##
 
* 1. Descargar y ejecutar el Web API de catalogo de libros: https://bitbucket.org/saheka14/servicioweblibros/src/master/
* 2. Compilar la aplicación con Visual Studio Code , descargar dependencias con npm install y ejecutarla, por default corre en el puerto 4200.
 
## Dependencias: ##

* Node.js v16.13.0
* Web API de catalogo de libros: https://bitbucket.org/saheka14/servicioweblibros/src/master/

##Credeciales default.##

* 1. usuario: admin
* 2. password: nokia3220

