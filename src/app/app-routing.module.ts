import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LibrosComponent } from './libros/libros.component';
import { LoginComponent } from './login/login.component';
import { VigilanteGuard } from './vigilante.guard';

const routes: Routes = [
  { path:'', component: LoginComponent},
   { 
     path:'libros', 
     component: LibrosComponent, 
     canActivate: [VigilanteGuard]
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
