import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginComponent } from './login/login.component';
import { URL_LOGIN } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AutenticationService {
  constructor(private http:HttpClient) {}

   login(data: any): Observable<any> {
      
      return this.http.post( URL_LOGIN, data);

   }
   
}
