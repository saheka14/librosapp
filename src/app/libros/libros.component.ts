import { Component, OnInit } from '@angular/core';
import { ViewChild , AfterViewInit} from '@angular/core';
import { MatTable} from '@angular/material/table';
import { MatTableDataSource} from '@angular/material/table';
import { MatDialog,MatDialogConfig } from '@angular/material/dialog';
import { RegistroComponent } from '../registro/registro.component';
import { MatSort } from '@angular/material/sort';
import { LibroService } from '../services/libro.service';
import { MatPaginator} from '@angular/material/paginator';


export interface LibrosElement {
  id: number;
  nombre: string;
  autor: string;
  anio: string;
  estatus: string;
}

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.css']
})
export class LibrosComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['id','nombre', 'autor', 'anio','estatus'];
  listData : MatTableDataSource<any>;
  searchKey:string;

  @ViewChild(MatTable) table: MatTable<LibrosElement>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.listData.paginator = this.paginator;
  }

  

  addData() {
    
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width ="30%";

    this.dialog.open(RegistroComponent, dialogConfig);
    this.table.renderRows();
  }


  constructor(
    private libroService: LibroService, 
    private dialog: MatDialog) { 

    this.cargarLibros();

  }

  cargarLibros(){
    this.libroService.getLibros().subscribe((respuesta:any )=> {
      if(respuesta.status == true)
      {
        console.log(respuesta.data.items);
        this.listData = new MatTableDataSource( respuesta.data.items);
        this.listData.sort = this.sort;
      }
    });
  }

  onSearchClear(){
    this.searchKey ="";
    this.applyFilter();
  }
  applyFilter(){
    this.listData.filter = this.searchKey.trim().toLowerCase();
    if (this.listData.paginator) {
      this.listData.paginator.firstPage();
    }
  }

  ngOnInit(): void {

  }
}
