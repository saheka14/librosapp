import { isGeneratedFile } from '@angular/compiler/src/aot/util';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AutenticationService } from '../autentication.service';
import { NotificacionService } from '../services/notificacion.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formGroup: FormGroup
  constructor(
    private authService: AutenticationService,
    private router: Router,
    private cookies: CookieService,
    public notificacionService: NotificacionService,
     ) { }

  ngOnInit(): void {
    this.initForm();
  }
  initForm(){
    this.formGroup = new FormGroup({
      usuario: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    })
  }

  loginProcess(){
    if(this.formGroup.valid){

      this.authService.login(this.formGroup.value).subscribe(
        result=>{ 
          
          if(result.message == "OK"){
            var credenciales = this.formGroup.value;
            
            this.cookies.set('credenciales',  btoa(credenciales.usuario + ':' + credenciales.password));
            this.cookies.set('acceso', result.message , 1 , '/');
            this.router.navigate(['/libros']);
          }

      }, error => {
          this.notificacionService.fail(error.error.message);
      })
    }
  }
}
