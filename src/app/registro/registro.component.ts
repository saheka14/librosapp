import { Component, OnInit } from '@angular/core';
import { LibroService } from '../services/libro.service';
import { MatDialogRef } from '@angular/material/dialog';
import { NotificacionService } from '../services/notificacion.service';
import { LibrosComponent } from '../libros/libros.component';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  constructor(
    public libroService: LibroService,
    public notificacionService: NotificacionService,
    public dialogRef: MatDialogRef<RegistroComponent>
    ) { }

    @ViewChild(LibrosComponent) librosTable: LibrosComponent;

  ngOnInit(): void {}

  onClose(){
    this.libroService.form.reset();
    this.libroService.initilizeFormGroup();
    this.dialogRef.close();
  }

  onSubmit(){
    if(this.libroService.form.valid){
      console.log(this.libroService.form.value);
      this.libroService.guardarLibro(this.libroService.form.value).subscribe((respuesta:any )=> {
        if(respuesta.status == true)
        {
    
          this.libroService.form.reset();
          this.libroService.initilizeFormGroup();
          this.notificacionService.success("Guardado con exitosamente.");
          this.librosTable.cargarLibros();
          this.onClose();
          
        }else{

          this.notificacionService.fail("Error al guardar.");
        }
      });
         }
  }



}
