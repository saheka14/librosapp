import { Injectable } from '@angular/core';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { URL_ADD, URL_GET } from 'src/environments/environment';
import { CookieService } from 'ngx-cookie-service';


@Injectable({
  providedIn: 'root'
})
export class LibroService {

  constructor(private httpClient: HttpClient, private cookieService:CookieService) { }

  form: FormGroup = new FormGroup({

    $id:new FormControl(null),
    nombre:new FormControl('',Validators.required),
    autor:new FormControl('',Validators.required),
    año:new FormControl('',[Validators.required,Validators.maxLength(4)]),
  });

  initilizeFormGroup(){
    this.form.setValue({
      $id:null,
      nombre:'',
      autor:'',
      año:''
    });
  }

  getLibros(){
  
    const headers = { 'Type-content':'aplication/json','Authorization':'Basic ' + this.cookieService.get('credenciales')  };
    const body = {};

     return this.httpClient.post(URL_GET,body,{headers});
   }

   guardarLibro(infoLibro : any){

    const headers = { 'Type-content':'aplication/json','Authorization':'Basic ' + this.cookieService.get('credenciales') };
    const body = { "nuevo_libro": { "libro_nombre":infoLibro.nombre,"libro_autor":infoLibro.autor,"libro_año":infoLibro.año}};
    return this.httpClient.post(URL_ADD, body,{headers});
   }

}
