import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {CookieService} from 'ngx-cookie-service'

@Injectable({
  providedIn: 'root'
})
export class VigilanteGuard implements CanActivate {

  constructor(private cookieService:CookieService, private router:Router){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
    const acceso = this.cookieService.get('acceso');
    this.redireccionar(acceso);
    return acceso == "OK" ? true : false;
  }

  redireccionar(acceso: string)
  {
    if(acceso != "OK"){
      this.router.navigate(['/']);
    }
  }
  
}
